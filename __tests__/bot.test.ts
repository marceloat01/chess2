import {Bot} from '../src/AI/Bot'
import * as ChessJS from "chess.js";

class BotTest extends Bot {
    public calculateMovesForPiece(game: ChessJS.Chess, square: string, color: string, piece: string): number {
        return super.calculateMovesForPiece(game, square, color, piece);
    }

    public calculateMaterial(fen: string, color: string): number {
        return super.calculateMaterial(fen, color);
    }
}

const botProperties = {
    depth: 1,
    level: 1,
    pieceWeight: 1
}

describe('Calculate moves for piece', () => {
    test('Calculate moves for bishop', () => {
        const fenEmptyBoard = [
            {fen: '3k4/8/8/8/3b4/8/8/3K4 w - - 0 1', color: 'b', startingAt: 'd4', freeSquares: 13},
            {fen: '3k4/8/8/2P1P3/3b4/2p1p3/8/3K4 w - - 0 1', color: 'b', startingAt: 'd4', freeSquares: 2},
            {fen: 'B4k2/8/8/8/8/8/8/2K5 w - - 0 1', color: 'w', startingAt: 'a8', freeSquares: 7},
        ]
        fenEmptyBoard.forEach((board) => {
            const game = new ChessJS.Chess(board.fen);
            const moves = new BotTest(botProperties).calculateMovesForPiece(
                game, board.startingAt, board.color, 'b');
            expect(moves).toBe(board.freeSquares);
        });
    })

    test('Calculate moves for rook', () => {
        const fenEmptyBoard = [
            {fen: '4k3/8/8/8/3R4/8/8/2K5 b - - 0 1', color: 'w', startingAt: 'd4', freeSquares: 14},
            {fen: '4k3/8/8/3P4/2PRp3/3p4/8/2K5 b - - 0 1', color: 'w', startingAt: 'd4', freeSquares: 2},
            {fen: '6kr/8/8/8/8/8/8/2K5 w - - 0 1', color: 'b', startingAt: 'h8', freeSquares: 7},
        ]
        fenEmptyBoard.forEach((board) => {
            const game = new ChessJS.Chess(board.fen);
            const moves = new BotTest(botProperties).calculateMovesForPiece(
                game, board.startingAt, board.color, 'r');
            expect(moves).toBe(board.freeSquares);
        });
    })

    test('Calculate moves for queen', () => {
        const fenEmptyBoard = [
            {fen: '5k2/8/8/3Q4/8/8/8/2K5 w - - 0 1', color: 'w', startingAt: 'd5', freeSquares: 27},
            {fen: '5k2/8/3ppP2/3Pqp2/3pPP2/8/8/2K5 b - - 0 1', color: 'w', startingAt: 'e5', freeSquares: 4},
        ]
        fenEmptyBoard.forEach((board) => {
            const game = new ChessJS.Chess(board.fen);
            const moves = new BotTest(botProperties).calculateMovesForPiece(
                game, board.startingAt, board.color, 'q');
            expect(moves).toBe(board.freeSquares);
        });
    })
});

describe('Calculate material', () => {
    test('Calculate material for white', () => {
        const fen = [
            {fen: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1', material: 39},
            {fen: 'r3kb1r/ppp3pp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 1', material: 39},
            {fen: '2bk1b2/8/8/8/8/8/3P4/3K4 w - - 0 1', material: 1},
        ]
        fen.forEach((board) => {
            const game = new ChessJS.Chess(board.fen);
            const material = new BotTest(botProperties).calculateMaterial(game.fen(), 'w');
            expect(material).toBe(board.material);
        });
    })

    test('Calculate material for black', () => {
        const fen = [
            {fen: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1', material: 39},
            {fen: 'r3kb1r/ppp3pp/8/8/8/8/PPPPPPPP/RNBQKBNR b KQkq - 0 1', material: 18},
            {fen: '2bk1b2/8/8/8/8/8/3P4/3K4 w - - 0 1', material: 6},
        ]
        fen.forEach((board) => {
            const game = new ChessJS.Chess(board.fen);
            const material = new BotTest(botProperties).calculateMaterial(game.fen(), 'b');
            expect(material).toBe(board.material);
        });
    })
})
