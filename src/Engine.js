import { useState, useEffect } from "react";
import * as ChessJS from "chess.js";
import { Chessboard } from "react-chessboard";
import { Bot } from './AI/Bot.ts';
import { Stockfish } from "./AI/Stockfish.ts";
import './App.css';

/* Mate in three
r2q1r1k/pbp1N1pp/1p1b4/5p2/2B5/P3PPn1/1P3P1P/2RQK2R w - - 0 1
1k5r/pP3ppp/3p2b1/1BN1n3/1Q2P3/P1B5/KP3P1P/7q w

Mate in two
r1bq2r1/b4pk1/p1pp1p2/1p2pP2/1P2P1PB/3P4/1PPQ2P1/R3K2R w
r1b2k1r/ppp1bppp/8/1B1Q4/5q2/2P5/PPP2PPP/R3R1K1 w
r1nk3r/2b2ppp/p3b3/3NN3/Q2P3q/B2B4/P4PPP/4R1K1 w

*/
export default function Engine(props) {
  const Chess = typeof ChessJS === "function" ? ChessJS : ChessJS.Chess;
  const [game, setGame] = useState(new Chess());
  let [result, setResult] = useState(null);
  const [player1, setPlayer1] = useState(null);
  const [player2, setPlayer2] = useState(null);
  const isPlayerHuman = props.w == 'human' || props.b == 'human';
  const [delay, setDelay] = useState(50);
  const [orientation, setOrientation] = useState("white");
  const timeTracker = [];

  useEffect(() => {
    timeTracker.length = 0;
    const initialiseBots = async () => {
      if (props.w === 'mbot') {
        setPlayer1(new Bot(props.p1.botProps));
      } else {
        setPlayer1(new Stockfish(props.p1.stockfishProps));
      }
      if (props.b === 'mbot') {
        setPlayer2(new Bot(props.p2.botProps));
      } else {
        setPlayer2(new Stockfish(props.p2.stockfishProps));
      }
      console.log(props.p1)
    }
    initialiseBots();
  }, []);

  const handleDelayChange = (event) => {
    setDelay(event.target.value);
  };

  props.onReturn(result);

  function makeAMove(move) {
    var moveExecuted = null;
    try {
        moveExecuted = game.move(move);
        setGame(new Chess(game.fen()));
        if (game.isGameOver()) {
          determineResult();
          console.log(`Length ${timeTracker.length} Max: ${Math.max(...timeTracker)} Min: ${Math.min(...timeTracker)} Avg: ${timeTracker.reduce((a, b) => a + b, 0) / timeTracker.length}`);

        }
        setTimeout(waitForMove, delay);
    } catch (e) {
        console.error("Invalid move", e);
        console.log("Move: ", move);
    }
    return moveExecuted;
  }

  function determineResult() {
    if (game.isDraw()) {
      _setResult(0);
      return;
    }
    if (game.turn() === 'w') {
      _setResult(-1);
    } else {
      _setResult(1);
    }
  }
  function _setResult(n) { setResult(n); result = n; }

  function onDrop(sourceSquare, targetSquare, x) {
    if (!isPlayerHuman || result) {
      return false;
    }
    const move = makeAMove({
      from: sourceSquare,
      to: targetSquare,
      promotion: "q", // always promote to a queen for example simplicity
    });

    if (move === null) {
        alert("Move is illegal")
        return false;
    } 
    return true;
  }

  async function waitForMove() {
    if (props[game.turn()] === 'human' || result != null) {
      return;
    }
    
    if (game.turn() == 'w') {
      player1.getBestMove(game.fen()).then((move) => makeAMove(move));
    } else {
      player2.getBestMove(game.fen()).then((move) => makeAMove(move));
    }
    
  }


  return (
    <div margin="100px">
        {props.children}
        <div style={{display: 'flex', justifyContent: 'center', margin: '10px'}}>
          <button className="google-button" onClick={waitForMove}>Start</button>
        </div>
        <Chessboard boardOrientation={orientation} areArrowsAllowed={true} position={game.fen()} onPieceDrop={onDrop} boardWidth={500} />
        <button class="flip-button" onClick={() => setOrientation(orientation === "white" ? "black" : "white")}>
          Flip
        </button>
        <TurnIndicator turn={game.turn()} />
        <div>Delay: <input type="number" min="50" max="2000" onChange={handleDelayChange} /></div>
    </div>
    );
}

function TurnIndicator(props) {
  if (props.turn === 'w') {
    return <div>White's turn</div>;
  } else {
    return <div>Black's turn</div>;
  }
}