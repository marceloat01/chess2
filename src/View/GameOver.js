import '../App.css';

export default function GameOverMessage({ result }) {
    if (result == 1) {
      return <div className='result'>White wins!</div>;
    } else if (result == -1) {
      return <div className='result'> Black wins!</div>;;
    } else if (result == 0) {
      return <div className='result'>It's a draw!</div>;
    }
    return <div></div>;
  }