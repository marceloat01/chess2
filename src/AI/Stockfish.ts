import { IBot } from "./IBot";

export interface StockfishProperties {
  stockfishLevel: number;
  stockfishDepth: number;
}

export class Stockfish implements IBot {
  private stockfish = 'stockfish/stockfish.js';
  private worker: Worker;
  private props = {stockfishLevel: 1, stockfishDepth: 1}

  constructor(props?: StockfishProperties) {
    if (props) {
      this.props = props;
    }
    this.worker = new Worker(this.stockfish);
  }

  public getBestMove(fen: string): Promise<string> {
    this.worker.postMessage('setoption name Skill Level value '+this.props.stockfishLevel)
    this.worker.postMessage('position fen ' + fen);
    this.worker.postMessage('go depth '+this.props.stockfishDepth)
    return this.waitForWorkerMessage(this.worker);
  }

  private waitForWorkerMessage(worker): Promise<string> {
    return new Promise((resolve, reject) => {
      function onMessage(e: MessageEvent) {
        const move = e.data.match(/^bestmove\s([a-h][1-8][a-h][1-8])/);
        if (move) {
          resolve(move[1]);
          worker.removeEventListener('message', onMessage);
        }
      }
      worker.addEventListener('message', onMessage);
    });
  }
}

