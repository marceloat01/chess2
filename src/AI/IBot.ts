import { Move } from 'chess.js';

export interface IBot {
    getBestMove(fen: string): Promise<String>;
}