import * as ChessJS from "chess.js";
import { IBot } from "./IBot";

export interface ChessMove {
    after?: string;
    before?: string;
    color?: string;
    flags?: string;
    from: string;
    lan?: string;
    piece?: string;
    san?: string;
    to: string;
    score: number;
  }

export interface BotProperties {
    depth: number,
    pieceWeight: number
}

interface MinimaxMove {
  move: ChessMove,
  score: number
}

export class Bot implements IBot{
  private props: BotProperties;
  private game: ChessJS.Chess;
  
  constructor(props: BotProperties) {
    this.props = props;
    console.log(this.props)
  }

  public getBestMove(fen: string): Promise<any> {
    return new Promise((resolve, reject) => {
        this.game = new ChessJS.Chess(fen);
        console.log(`Before White: ${this.evaluatePosition(this.game, 'w')} Black: ${this.evaluatePosition(this.game, 'b')}`);
        const bestMove = this.minimax(this.game, this.props.depth, -Infinity, Infinity,  'w', this.getPossibleMoves(this.game)[0]);
        console.log(`After White: ${this.evaluatePosition(this.game, 'w')} Black: ${this.evaluatePosition(this.game, 'b')}`);
        resolve(bestMove.move);
      });
  }

  private getPossibleMoves(game: ChessJS.Chess): ChessMove[] {
    const moves = game.moves({verbose: true})
      .map((move: ChessJS.Move): ChessMove => (
          {...move, before: game.fen(), after: game.fen(), lan: move.san, score: 0}
      ));
   return this.sortByLan(moves);
  }

  private minimax(game: ChessJS.Chess, depth: number, alpha: number, beta: number, maximizingPlayer: ChessJS.Color, move: ChessMove): MinimaxMove {
    if (depth == 0 || game.isGameOver()) {
      const score = this.evaluatePosition(game, 'w') - this.evaluatePosition(game, 'b')
      return {
        score: score,
        move: move
      }
    }

    var possibleMoves = this.getPossibleMoves(game);
    var bestMove = possibleMoves[0];
    var mEval = -Infinity;

    if (game.turn() === maximizingPlayer) {
      possibleMoves.some((nextMove) => {
        var gameCopy = new ChessJS.Chess(game.fen());
        gameCopy.move(nextMove);
        const nextMinimax = this.minimax(gameCopy, depth - 1, alpha, beta, maximizingPlayer, nextMove);
        mEval = Math.max(mEval, nextMinimax.score);
        if (mEval === nextMinimax.score) {
          bestMove = nextMove;
        }
        alpha = Math.max(alpha, mEval);
        if (beta <= alpha) {
         return true;
        }
        return false;
      });
    } else {
      mEval = Infinity;
      possibleMoves.some((nextMove) => {
        var gameCopy = new ChessJS.Chess(game.fen());
        gameCopy.move(nextMove);
        const nextMinimax = this.minimax(gameCopy, depth - 1, alpha, beta, maximizingPlayer, nextMove);
        mEval = Math.min(mEval, nextMinimax.score);
        if (mEval === nextMinimax.score) {
          bestMove = nextMove;
        }
        beta = Math.min(beta, mEval);
        if (beta <= alpha) {
         return true;
        }
        return false;

      });
    }

    return {
      score: mEval,
      move: bestMove
    } 
  }

  private evaluatePosition(game: ChessJS.Chess, color: ChessJS.Color): number {
    let score = 0;
    if (game.isCheckmate()) {
      return color === 'w' ? Infinity : -Infinity;
    }
    ['b', 'q', 'r'].map(p => this.findPieces(game, p, color).forEach((position) => {
      score += this.calculateMovesForPiece(game, position, color, p);
    }));
    score += this.calculateMaterial(game.fen(), color);
    return score;
  }

  private findPieces(game: ChessJS.Chess, type: string, color: string): string[] {
    const board = game.board();
    let positions: string[] = [];
  
    for (let row = 0; row < 8; row++) {
      for (let col = 0; col < 8; col++) {
        const piece = board[row][col];
        if (piece && piece.type === type && piece.color === color) {
          // Convert row and col to chess notation (e.g., "e5")
          const square = String.fromCharCode('a'.charCodeAt(0) + col) + (8 - row);
          positions.push(square);
        }
      }
    }
    return positions;
  }

  protected calculateMovesForPiece(game: ChessJS.Chess, startingSquare: String, color: String, piece = 'b') {
    const board = game.board();
    const startCol = 'abcdefgh'.indexOf(startingSquare[0])
    const startRow = 8-parseInt(startingSquare[1])
    let totalCounter = 0;

    // Define the directions: up-right, up-left, down-right, down-left (bishop)
    const directions = {
      'b': [[-1, 1], [-1, -1], [1, 1], [1, -1]], // Bishop
      'r': [[-1, 0], [1, 0], [0, -1], [0, 1]],   // Rook
      'q': [[-1, 1], [-1, -1], [1, 1], [1, -1], [-1, 0], [1, 0], [0, -1], [0, 1]] // Queen
    }[piece] || []; // Default to bishop if unknown piece

   for (let i = 0; i < directions.length; i++) {
        const dx = directions[i][0];
        const dy = directions[i][1];
        let row = startRow + dx;
        let col = startCol + dy;
        let counter = 0;

        // Loop through board in direction (dx, dy)
        while (row >= 0 && row < 8 && col >= 0 && col < 8) {
            const currentPiece = board[row][col];
            if (currentPiece) {
                if (currentPiece.color !== color) {
                    counter++;
                }
                break;
            }
            row += dx;
            col += dy;
            counter++;
        }

        totalCounter += counter;
    }
    return totalCounter;
  }

  protected calculateMaterial(fen: String, color: String = 'w') {
    var pieces: string[] = fen.slice(0, fen.indexOf(' ')).replace(/r|n|b|q|p/g, '').split('').map(p => p.toLowerCase())
    if (color === 'b') {
      pieces = fen.slice(0, fen.indexOf(' ')).replace(/R|N|B|Q|P/g, '').split('')
    }
    return pieces.filter(char => char === 'n' || char === 'b').length*3*this.props.pieceWeight
    + pieces.filter(char => char === 'p').length*this.props.pieceWeight
    + pieces.filter(char => char === 'r').length*5*this.props.pieceWeight
    + pieces.filter(char => char === 'q').length*9*this.props.pieceWeight
  }

  private sortByLan(moves) {
    return moves.sort((a, b) => {
      const containsHashA = a.lan && a.lan.indexOf('#') > -1;
      const containsHashB = b.lan && b.lan.indexOf('#') > -1;
      const containsPlusA = a.lan && a.lan.indexOf('+') > -1;
      const containsPlusB = b.lan && b.lan.indexOf('+') > -1;
      const containsCrossA = a.lan && a.lan.indexOf('x') > -1;
      const containsCrossB = b.lan && b.lan.indexOf('x') > -1;
  
      if (containsHashA && !containsHashB) {
        return -1;
      } else if (!containsHashA && containsHashB) {
        return 1;
      } else if (containsPlusA && !containsPlusB) {
        return -1;
      } else if (!containsPlusA && containsPlusB) {
        return 1;
      }  else if (containsCrossA && !containsCrossB) {
        return -1;
      } else if (!containsCrossA && containsCrossB) {
        return 1;
      } else {
        return 0;
      }
    });
  }
  
}
