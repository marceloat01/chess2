import React, { useState } from 'react';
import Engine from "./Engine";
import GameOverMessage from "./View/GameOver";
import './App.css';

export default function App() {
  const [result, setResult] = useState(null);
  const [engineKey, setEngineKey] = useState(0);
  const [player1, setPlayer1] = useState('mbot');
  const [player2, setPlayer2] = useState('stockfish');
  let player1Props = {stockfishProps: initialiseStockfishProps(), botProps: initialiseBotProps()};
  let player2Props = {stockfishProps: initialiseStockfishProps(), botProps: initialiseBotProps()};

  const refreshEngine = () => {
    setEngineKey(prevKey => prevKey + 1);
  };

  const handleResult = (result) => {
    setResult(result);
  };

  function initialiseStockfishProps() {
    return {
      stockfishLevel: 0,
      stockfishDepth: 0
    }
  }

  function initialiseBotProps() {
    return {
      depth: 1,
      pieceWeight: 1
    }
  }

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <h1>Chess Bot</h1>
      
        <div className="app-container">
          <div className="player-selection">
              <div className="player">
                <label htmlFor="player1">White:</label>
                <select id="player1" value={player1} onChange={(event) => setPlayer1(event.target.value)}>
                    <option value="human">Human</option>
                    <option value="mbot">MBot</option>
                    <option value="stockfish">Stockfish</option>
                </select>
                
                <div className="stockfish-settings" style={{display: player1 !== 'stockfish' ? 'none' : 'block'}}>
                  <div className="setting">
                    <label htmlFor="stockfishLevel">Stockfish level (0-20):</label>
                    <input type="number" id="stockfishLevel" min="0" max="20" defaultValue={0} onChange={(event) => player1.stockfishProps.stockfishLevel = event.target.value}/>
                  </div>
                  <div className="setting">
                    <label htmlFor="stockfishDepth">Stockfish depth (1-10):</label>
                    <input type="number" id="stockfishDepth" min="1" max="10" defaultValue={0} onChange={(event) => player1.stockfishProps.stockfishDepth = event.target.value}/>
                  </div>
                </div>

                <div className="mbot-settings" style={{display: player1 !== 'mbot' ? 'none' : 'block'}}>
                    <div className="setting">
                      <label htmlFor="botDepth">MBot depth:</label>
                      <input type="number" id="botDepth" min="0" max="10" defaultValue={1} onChange={(event) => player1.botProperties.depth = event.target.value}/>
                    </div>
                    <div className="setting">
                      <label htmlFor="botPieceWeight">MBot depth:</label>
                      <input type="number" id="botPieceWeight" min="0" max="10" defaultValue={1} onChange={(event) => player1.botProperties.pieceWeight = event.target.value}/>
                    </div>
                  </div>
              </div>
              
              <div className="player">
                <label htmlFor="player2">Black:</label>
                <select id="player2" value={player2} onChange={(event) => setPlayer2(event.target.value)}>
                    <option value="human">Human</option>
                    <option value="mbot">MBot</option>
                    <option value="stockfish">Stockfish</option>
                </select>
                
                <div className="stockfish-settings" style={{display: player2 !== 'stockfish' ? 'none' : 'block'}}>
                  <div className="setting">
                    <label htmlFor="stockfishLevel">Stockfish level (0-20):</label>
                    <input type="number" id="stockfishLevel" min="0" max="20" defaultValue={0} onChange={(event) => player2.stockfishProps.stockfishLevel = event.target.value}/>
                  </div>
                  <div className="setting">
                    <label htmlFor="stockfishDepth">Stockfish depth (1-10):</label>
                    <input type="number" id="stockfishDepth" min="1" max="10" defaultValue={0} onChange={(event) => player2.stockfishProps.stockfishDepth = event.target.value}/>
                  </div>
                </div>

                <div className="mbot-settings" style={{display: player2 !== 'mbot' ? 'none' : 'block'}}>
                  <div className="setting">
                    <label htmlFor="botDepth">MBot depth:</label>
                    <input type="number" id="botDepth" min="0" max="10" defaultValue={1} onChange={(event) => player2.botProperties.depth = event.target.value}/>
                  </div>
                  <div className="setting">
                    <label htmlFor="botPieceWeight">MBot depth:</label>
                    <input type="number" id="botPieceWeight" min="0" max="10" defaultValue={1} onChange={(event) => player2.botProperties.pieceWeight = event.target.value}/>
                  </div>
                </div>
            </div>
          </div>        
        </div>

        <button className="google-button" onClick={refreshEngine}>Refresh</button>
        <div className="board">
          <Engine 
            key={engineKey}
            onReturn={handleResult} 
            w={player1}
            b={player2}
            p1={player1Props}
            p2={player2Props}
            >
                <GameOverMessage result={result} />
            </Engine>
        </div>
    </div>
  );
}
